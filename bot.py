import discord
from discord.ext import commands
from pretty_help import PrettyHelp

client = commands.Bot(
    command_prefix="m/",
    help_command=PrettyHelp(color=discord.Color.purple(), active=5), # NOTE: message will be active for 5s
)


@client.event
async def on_ready():
    await client.change_presence(status=discord.Status.idle,
    activity=discord.Game('m/help'))
    print('Bot is ready.')

@client.event
async def on_member_join(member):
    print(f'{member} has joined the server!')

@client.event
async def on_member_remove(member):
    print(f'{member}has left the server :<')

@client.command()
async def ping(ctx):
    await ctx.send(f'Pong! Client side ping - {round(client.latency * 1000)}ms')

@client.command(aliases=["whois"])
async def userinfo(ctx, member: discord.Member = None):
    if not member:  # if member is no mentioned
        member = ctx.message.author  # set member as the author
    roles = [role for role in member.roles]
    embed = discord.Embed(colour=discord.Colour.purple(), timestamp=ctx.message.created_at,
                          title=f"User Info - {member}")
    embed.set_thumbnail(url=member.avatar_url)
    embed.set_footer(text=f"Requested by {ctx.author}")

    embed.add_field(name="ID:", value=member.id)
    embed.add_field(name="Display Name:", value=member.display_name)

    embed.add_field(name="Created Account On:", value=member.created_at.strftime("%a, %#d %B %Y, %I:%M %p UTC"))
    embed.add_field(name="Joined Server On:", value=member.joined_at.strftime("%a, %#d %B %Y, %I:%M %p UTC"))
    embed.add_field(name="Roles:", value="".join([role.mention for role in roles]))
    print(member.top_role.mention)
    await ctx.send(embed=embed)


@client.event
async def on_command_error(ctx, error):
    if isinstance(error, commands.MissingRequiredArgument):
        await ctx.send('Please pass in all requirements :rolling_eyes:.')
    if isinstance(error, commands.MissingPermissions):
        await ctx.send("You dont have all the requirements :angry:")

#The below code bans player.
@client.command()
@commands.has_permissions(ban_members = True)
async def ban(ctx, member : discord.Member, *, reason = None):
    await member.ban(reason = reason)
    await ctx.send(f'Banned {user.mention}')

#The below code unbans player.
@client.command()
@commands.has_permissions(administrator = True)
async def unban(ctx, *, member):
    banned_users = await ctx.guild.bans()
    member_name, member_discriminator = member.split("#")

    for ban_entry in banned_users:
        user = ban_entry.user

        if (user.name, user.discriminator) == (member_name, member_discriminator):
            await ctx.guild.unban(user)
            await ctx.send(f'Unbanned {user.mention}')
            return

@client.command(aliases=["avi"])
async def avatar(ctx, member: discord.Member = None):
    if not member:
        member = ctx.message.author
    embed = discord.Embed(colour=discord.Colour.purple(), timestamp=ctx.message.created_at,
                          title=f"Avatar - {member}")
    embed.set_image(url=member.avatar_url)
    await ctx.send(embed=embed)

@client.command(pass_context=True)
@commands.has_permissions(administrator=True)
async def clear(ctx, arg: int):
    if not arg:
        embed = discord.Embed(
            color=discord.Colour.purple()
        )
        embed.set_author(
            name="Cleared selected amount of messages",
        )
        await ctx.send(embed=embed, delete_after=10.0)
        return
    embed = discord.Embed(
        color=discord.Colour.purple()
    )
    embed.set_author(
        name=f'Cleared {arg} message(s)',
        icon_url=f'{ctx.author.avatar_url}'
    )
    await ctx.channel.purge(limit=arg+1)
    await ctx.send(embed=embed, delete_after=10.0)
    embed = discord.Embed(
        color=discord.Colour.purple()
    )
    embed.set_author(
        name=f'{ctx.author._user} ha cancellato {arg}',
        icon_url=f'{ctx.author.avatar_url}'
    )
    embed.add_field(
        name='Messaggi cancellati da:',
        value=f'{ctx.author._user}',
        inline=True
    )
    embed.add_field(
        name='Quantità:',
        value=f'{arg}',
        inline=True
    )
    channel = client.get_channel(729553772547932190)
    await channel.send(embed=embed)
@clear.error
async def clear_error(ctx, error):
    if isinstance(error, commands.CheckFailure):
        embed.set_author(
            name="You do not have the required perms to do this command!",
            icon_url='your image'
        )
    elif isinstance(error, commands.MissingRequiredArgument):
        embed.set_author(
            name="No args given!",
            icon_url='your image'
        )
    await ctx.send(embed=embed, delete_after=2.50)


@client.command()
@commands.has_permissions(kick_members=True)
async def kick(ctx, member : discord.Member, *, reason=None):
    await member.kick(reason=reason)
    embed=discord.Embed(title="Kicked user.", color=discord.Colour.purple())
    embed.add_field(name="Display Name:", value=member.display_name)
    embed.set_thumbnail(url=member.avatar_url)
    embed.add_field(name="Reason:", value=reason)
    embed.add_field(name="Joined Server On:", value=member.joined_at.strftime("%a, %#d %B %Y, %I:%M %p UTC"))
    await ctx.send(embed=embed)



@client.command()
async def mute(ctx, member: discord.Member):
    role = discord.utils.get(ctx.guild.roles, name='Muted')
    await member.add_roles(role)
    embed=discord.Embed(title="Muted user.", color=discord.Colour.purple())
    embed.add_field(name="Display Name:", value=member.display_name)
    embed.set_thumbnail(url=member.avatar_url)
    await ctx.send(embed=embed)


@client.command()
async def unmute(ctx, member: discord.Member):
    role = discord.utils.get(ctx.guild.roles, name='Muted')
    await member.remove_roles(role)
    embed=discord.Embed(title="Unmuted user.", color=discord.Colour.purple())
    embed.add_field(name="Display Name:", value=member.display_name)
    embed.set_thumbnail(url=member.avatar_url)
    await ctx.send(embed=embed)


@client.command(aliases=["gh"])
async def github(ctx, member: discord.Member = None):
    embed = discord.Embed(colour=discord.Colour.purple(), timestamp=ctx.message.created_at,
                          title=f"GitHub for this project: https://github.com/rosekeyq/modbot.py")
    embed.set_footer(text=f"Requested by {ctx.author}")
    await ctx.send(embed=embed)


@client.command(aliases=["server-info"])
async def serverinfo(ctx, member: discord.Member = None):

    embed = discord.Embed( timestamp=ctx.message.created_at, color=discord.Colour.purple())
    embed.set_author(name=f"Information for: {ctx.guild.name}")
    embed.add_field(name="Server ID:", value= f"{ctx.guild.id}")
    embed.add_field(name="Server owner:", value = f" {ctx.guild.owner}")
    embed.add_field(name="Server created:", value=f" {ctx.guild.created_at}")
    embed.add_field(name="Member count:", value=f" {ctx.guild.member_count}")
    embed.add_field(name="Channel count:", value=f" {len(ctx.guild.text_channels)}")
    embed.set_footer(text=f"Requested by {ctx.author}")
    await ctx.send(embed=embed)

@client.command(aliases=["doc"])
async def docs(ctx,*,arg):
    distros = {
    "ubuntu": "https://docs.ubuntu.com/",
    "arch": "https://wiki.archlinux.org/",
    "gentoo": "https://wiki.gentoo.org/wiki/Main_Page",
    "fedora": "https://docs.fedoraproject.org/en-US/docs/",
    "debian": "https://www.debian.org/doc/",
    "manjaro": "https://wiki.manjaro.org",
    "opensuse": "https://doc.opensuse.org/",
    "kali": "https://www.kali.org/docs/",
    "zorin": "https://zorinos.com/",
    "mint": "https://linuxmint.com/documentation.php",
    "venom": "https://osdn.net/projects/venomlinux/wiki/FrontPage",
    "elementary": "https://elementary.io/docs",
    "clear": "https://docs.clearos.com/en"
    }
    distro = distros[arg]
    if arg in distros:
        await ctx.send(f"Here are the officials docs for {arg}: {distro}")

@docs.error
async def docs_error(ctx,error) :
    if isinstance(error, commands.MissingRequiredArgument):
            author = ctx.message.author
            embed = discord.Embed(
            colour = discord.Colour.purple()
            )
            embed.set_author(name='Available distro docs:')
            embed.add_field(name='Ubuntu',value = "ubuntu",inline=False)
            embed.add_field(name='Debian',value = "debian", inline=False)
            embed.add_field(name='Arch',value = "arch",inline=False)
            embed.add_field(name='Fedora',value = "fedora",inline=False)
            embed.add_field(name='Gentoo',value = "gentoo",inline=False)
            embed.add_field(name='Zorin',value = "zorin",inline=False)
            embed.add_field(name="Manjaro",value="manjaro",inline=False)
            embed.add_field(name="openSUSE",value="opensuse",inline=False)
            embed.add_field(name="Kali",value="kali",inline=False)
            embed.add_field(name="Mint",value="mint",inline=False)
            embed.add_field(name="Venom",value="venom",inline=False)
            embed.add_field(name="ElementaryOS",value="elementary",inline=False)
            embed.add_field(name="ClearOS",value="clear",inline=False)
            await ctx.send(embed=embed)     # Thank you to Abbix for providing this code!

@client.command()
async def creator(ctx, member: discord.Member = None):
    await ctx.send('The creator of Tuxs Security is Mabel#0001.')


@client.command(aliases=["lc"])
async def license(ctx, member: discord.Member = None):
    embed = discord.Embed(colour=discord.Colour.purple(), timestamp=ctx.message.created_at,
                          title=f"GNU GENERAL PUBLIC LICENSE, read more here: https://www.gnu.org/licenses/gpl-3.0.en.html")
    embed.set_footer(text=f"Requested by {ctx.author}")
    await ctx.send(embed=embed)   
    






client.run('?')
